package multiset;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 * <p>A MultiSet models a data structure containing elements along with their frequency count i.e., </p>
 * <p>the number of times an element is present in the set.</p>
 * <p>HashMultiSet is a Map-based concrete implementation of the MultiSet concept.</p>
 * 
 * <p>MultiSet a = <{1:2}, {2:2}, {3:4}, {10:1}></p>
 * */
public final class HashMultiSet<T, V> {

	/**
	 *XXX: data structure backing this MultiSet implementation. 
	 */
	 HashMap<T,V> counter;
	/**
	 * Sole constructor of the class.
	 **/
	public HashMultiSet() {counter= new HashMap<T,V>();}
	
	
	/**
	 * If not present, adds the element to the data structure, otherwise 
	 * simply increments its frequency.
	 * 
	 * @param t T: element to include in the multiset
	 * 
	 * @return V: frequency count of the element in the multiset
	 * */	
	public V addElement(T t) {
		Integer value = (Integer)counter.get(t);
		if(value == null) {		
			counter.put(t,(V)value);
			value = 1;
			
		}
		else {		
	    	value++;
		}
		return value;	
	}

	/**
	 * Check whether the elements is present in the multiset.
	 * 
	 * @param t T: element
	 * 
	 * @return V: true if the element is present, false otherwise.
	 * */	
	public boolean isPresent(T t){	
		if((Integer)counter.get(t) != null)
			return true;
		return false;
	}
	
	/**
	 * @param t T: element
	 * @return V: frequency count of parameter t ('0' if not present)
	 * */
	public V getElementFrequency(T t){	
	
			return counter.get(t)
	}
	
	
	/**
	 * Builds a multiset from a source data file. The source data file contains
	 * a number comma separated elements. 
	 * Example_1: ab,ab,ba,ba,ac,ac -->  <{ab:2},{ba:2},{ac:2}>
	 * Example 2: 1,2,4,3,1,3,4,7 --> <{1:2},{2:1},{3:2},{4:2},{7:1}>
	 * 
	 * @param source Path: source of the multiset
	 * */
	public void buildFromFile(Path source) throws IOException {
		
		
	}

	/**
	 * Same as before with the difference being the source type.
	 * @param source List<T>: source of the multiset
	 * */
	public void buildFromCollection(List<? extends T> source) {
		if(source==null) throw new IllegalArgumentException("wrong invocation");
		source.stream().forEach(x->addElement(x))
	}
	
	/**
	 * Produces a linearized, unordered version of the MultiSet data structure.
	 * Example: <{1:2},{2:1}, {3:3}> -> 1 1 2 3 3 3 3
	 * 
	 * @return List<T>: linearized version of the multiset represented by this object.
	 */
	public List<T> linearize() {
		List<T> final;
		
		for(T t: counter){
			Integer count = (Integer)getElementFrequency(t);
			for(Integer i: count)
				final.add((T)i);
		}
			
	}
	return final;
	
}
